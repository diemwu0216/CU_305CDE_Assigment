
var mongoose = require('mongoose')
const server = 'mongodb://diem:diem@ds141950.mlab.com:41950/assigement'
console.log('connect to server: '+server)
mongoose.connect(server)
const db = mongoose.connection
var Schema = mongoose.Schema

// user Schema
var userSchema = new mongoose.Schema({
    UserName: { type: String, required: true, index: { unique: true } },
    UserPw: { type: String, required: true }
})
// diary Schema
var diaryScheam = new mongoose.Schema({
    UserName: { type: String, required: true, index: { unique: true } },
    Country: { type: String, required: true },
    Date: { type: String, required: true },
    weather: [{ 
      temp:{type: String, required: true},
      main:{type: String, required: true}
    }],
    Title: { type: String, required: true },
    content: { type: String, required: true }
})

exports.User = mongoose.model('users', userSchema) // user table
exports.diary = mongoose.model('diary', diaryScheam) // diary table
exports.diaryColl =  db.collection('diary')

exports.addDiary = function(json){ // addDiary
  db.collection('diary').insert(json)
}
exports.addUser = function(json){ // add User
  db.collection('users').insert(json)
}
// exports.addItem = function(json) { 
//   db.collection('list2').insert(json)
// }

/* Diary 
 Function 
 1) get Weather (JSON) 
 2) get daily (DB Json Array 1:weather 2:desctiption)
 2) login (DB) 
 3) add diray
    A) first choose location
    B) insert text
    C) update to database
 4) Register 
 */
