'use strict'
/*istanbul ignore next*/
/* global expect */
var fs = require('fs');

var diary = require("../diary.js")
var users = require('../user.js')
var weather = require('../weather.js')

// Test add new user *******
var addUserbody = fs.readFileSync('./testJson/addUser.json'); // Get New user json
    const data = users.addUser(addUserbody,function(data) { // Call user.js addUser function
      if(data.code == "200" || data.code == "201" ){ //create successful
        console.log("sucessful add user")
        console.log("-------------------------------------------")
      }else{
        //create fail
        console.log("error: add user")
        console.log("-------------------------------------------")
      }
  })

// Test Login *******
 var LoginJSONbody = fs.readFileSync('./testJson/Login.json');
    const LoginData = users.Login(LoginJSONbody,function(LoginData) { // Call user.js Login function

      if(LoginData.code == "200" || LoginData.code == "201" ){// Login successful
        console.log("sucessful Login")
        console.log("-------------------------------------------")
      }else{
        console.log("error: login")
        console.log("-------------------------------------------")
      }
  })
    
// Test add new diary *******
 var addNewDiarybody = fs.readFileSync('./testJson/addNewDiary.json');
    const addNewDiaryData = diary.addDiary(addNewDiarybody,function(addNewDiaryData) { // Call diary.js addDiary function

      if(addNewDiaryData.code == "200" || addNewDiaryData.code == "201" ){ //add diary successful
        console.log("sucessful add New Diary")
        console.log("-------------------------------------------")
      }else{
        // Get Fail
        console.log("error: add New Diary")
        console.log("-------------------------------------------")
      }
  })
// Test get weather *******
 var Country = "Hong Kong"
    const getWeatherData = weather.getByCountry(Country,function(addNewDiaryData) { // Call weather.js getByCountry function

      if(addNewDiaryData.code == "200" || addNewDiaryData.code == "201" ){ //get weather successful
        console.log("sucessful get country weather")
        console.log(addNewDiaryData.response)
        console.log("-------------------------------------------")
      }else{
        // Get Fail
        console.log("error: add New Diary")
        console.log("-------------------------------------------")
      }
  })
